"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.safeAsync = exports.isSafeAsyncError = void 0;
const shortid_1 = __importDefault(require("shortid"));
const react_1 = require("react");
const SAFE_ASYNC_ERROR = {
    // Backwards compatibility
    isCanceled: true,
    hasCanceled: true
};
function isSafeAsyncError(error) {
    return !!error && (!!error.isCanceled || !!error.hasCanceled);
}
exports.isSafeAsyncError = isSafeAsyncError;
function safeAsync() {
    let subscribers = [];
    function removePromiseByID(_id) {
        if (subscribers) {
            subscribers = subscribers.filter(cPromise => cPromise._id !== _id);
        }
    }
    function cancelablePromise(promise) {
        const _id = shortid_1.default.generate();
        let hasCanceled_ = false;
        const wrappedPromise = new Promise(async (resolve, reject) => {
            function rejectPromise(error) {
                removePromiseByID(_id);
                return hasCanceled_ ? reject(SAFE_ASYNC_ERROR) : reject(error);
            }
            function resolvePromise(_promise) {
                if (hasCanceled_) {
                    return rejectPromise(reject);
                }
                removePromiseByID(_id);
                return resolve(_promise);
            }
            if (!promise || !promise.then) {
                return resolvePromise(promise);
            }
            return promise.then(value => resolvePromise(value), error => rejectPromise(error));
        });
        return {
            _id,
            promise: wrappedPromise,
            cancel() {
                hasCanceled_ = true;
            }
        };
    }
    return {
        add: (promise) => {
            const cPromise = cancelablePromise(promise);
            subscribers.push({ ...cPromise });
            return cPromise.promise;
        },
        cancelPendingRequest: () => {
            if (!subscribers) {
                throw SAFE_ASYNC_ERROR;
            }
            subscribers = subscribers.filter(subscriber => {
                subscriber.cancel();
                return false;
            });
        }
    };
}
exports.safeAsync = safeAsync;
const useSafeAsync = () => react_1.useRef(safeAsync()).current;
exports.default = useSafeAsync;
