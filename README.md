# README

Khemlabs - SafeAsyncHook

## Description

Cancelable promises to prevent memory leak due unmounted component in react native

## Usage

Add dependency:

```sh
yarn add git+https://bitbucket.org/khemlabs/safe-async-hook.git#0.1.1
```

Import hook:

```js
import useSafeAsync, {isSafeAsyncError} from "safe-async-hook";
```

Use hook:

```js
const safeAsync = useSafeAsync();

try {
  const asyncResponse = await safeAsync.add(asyncMethod());
} catch (error) {
  // cancelPendingRequest was called
  if (isSafeAsyncError(error)) {
    return;
  }
}
```

## Example

```js
// Import react hooks
import React, { useEffect, useState } from "react";
import { Text } from "react-native";

// Import hook
import useSafeAsync, {isSafeAsyncError} from 'safe-async-hook';

// Simulate async data
function getAsyncNumbers() {
  return new Promise((resolve) => {
    setTimeout(() => {
      return resolve([1, 2, 3, 4]);
    }, 1000);
  });
}

// Your component
const MyComponent = () => {
  const safeAsync = useSafeAsync();

  const [numbers, setNumbers] = useState([]);

  useEffect(() => {
    const loadNumbers = async () => {
      try {
        const asyncNumbers = await safeAsync.add(getAsyncNumbers());
        setNumbers(asyncNumbers);
      } catch (error) {
        // cancelPendingRequest was called
        if (isSafeAsyncError(error)) {
          return;
        }

        // Handle error
        console.error(error);
      }
    };

    loadNumbers();

    // Cancel all pending requests on component unmount
    return () => safeAsync.cancelPendingRequest();
  }, [safeAsync]);

  return <Text>{numbers.join(", ")}</Text>;
};

export default MyComponent;
```

```ts
// Import react hooks
import React, { useEffect, useState } from "react";
import { Text } from "react-native";

// Import hook
import useSafeAsync, {isSafeAsyncError} from 'safe-async-hook';

// Simulate async data
function getAsyncNumbers(): Promise<number[]> {
  return new Promise((resolve) => {
    setTimeout(() => {
      return resolve([1, 2, 3, 4]);
    }, 1000);
  });
}

// Your component
const MyComponent = () => {
  const safeAsync = useSafeAsync();

  const [numbers, setNumbers] = useState<number[]>([]);

  useEffect(() => {
    const loadNumbers = async () => {
      try {
        // asyncNumbers will be of type number[]
        const asyncNumbers = await safeAsync.add(getAsyncNumbers());
        setNumbers(asyncNumbers);
      } catch (error) {
        // cancelPendingRequest was called
        if (isSafeAsyncError(error)) {
          return;
        }
        // Handle error
        console.error(error);
      }
    };

    loadNumbers();

    // Cancel all pending requests on component unmount
    return () => {
      safeAsync.cancelPendingRequest();
    }
  }, [safeAsync]);

  return <Text>{numbers.join(", ")}</Text>;
};

export default MyComponent;
```

## Who do I talk to?

- [dnangelus](https://github.com/DNAngeluS) repo owner and admin
- [elgambet](https://github.com/elgambet) developer and admin
