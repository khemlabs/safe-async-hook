export function isSafeAsyncError(error: any): boolean;

export type SafeAsyncReturnType = {
	add<T>(promise: Promise<T>): Promise<T>;
	cancelPendingRequest(): void;
};

export default function safeAsync(): SafeAsyncReturnType;
