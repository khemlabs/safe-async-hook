import shortid from 'shortid';
import { useRef } from 'react';

type CancelablePromise<T> = {
	_id: string;
	promise: Promise<T>;
	cancel: () => void;
}

const SAFE_ASYNC_ERROR = {
	// Backwards compatibility
	isCanceled: true,
	hasCanceled: true
}

export function isSafeAsyncError(error: any): boolean {
	return !!error && (!!error.isCanceled || !!error.hasCanceled)
}

export function safeAsync<T>() {
	let subscribers: CancelablePromise<T>[] = [];

	function removePromiseByID(_id: string) {
		if (subscribers) {
			subscribers = subscribers.filter(cPromise => cPromise._id !== _id);
		}
	}

	function cancelablePromise(promise: Promise<T>): CancelablePromise<T> {
		const _id = shortid.generate();

		let hasCanceled_ = false;

		const wrappedPromise: Promise<T> = new Promise(async (resolve, reject) => {
			function rejectPromise(error: any) {
				removePromiseByID(_id);
				return hasCanceled_ ? reject(SAFE_ASYNC_ERROR) : reject(error);
			}
	
			function resolvePromise(_promise: Promise<T> | T) {
				if (hasCanceled_) {
					return rejectPromise(reject);
				}
				removePromiseByID(_id);
				return resolve(_promise);
			}
			
			if (!promise || !promise.then) {
				return resolvePromise(promise);
			}

			return promise.then(value => resolvePromise(value), error => rejectPromise(error));
		});

		return {
			_id,
			promise: wrappedPromise,
			cancel() {
				hasCanceled_ = true;
			}
		};
	}

	return {
		add: (promise: Promise<T>) => {
			const cPromise = cancelablePromise(promise);
			subscribers.push({ ...cPromise });
			return cPromise.promise;
		},
		cancelPendingRequest: () => {
			if (!subscribers) {
				throw SAFE_ASYNC_ERROR;
			}
			subscribers = subscribers.filter(subscriber => {
				subscriber.cancel();
				return false;
			});
		}
	};
}

const useSafeAsync = () => useRef(safeAsync()).current;

export default useSafeAsync;
