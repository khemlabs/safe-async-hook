const useSafeAsyncLib = require('../build/index');

const useSafeAsync = useSafeAsyncLib.safeAsync;
const isSafeAsyncError = useSafeAsyncLib.isSafeAsyncError;

const safeAsync = useSafeAsync();

const objectToReturn = {success: 'yes'};

function returnsObject() {
    return new Promise(resolve => {
        setTimeout(() => resolve(objectToReturn), 100);
    });
}

function throwException() {
    return new Promise((_,reject) => {
        setTimeout(() => reject(false), 100);
    });
}

async function throwSafeAsyncException() {
    const object = await returnsObject();
    return object;
}

test('Creates an async promise and expects valid object', async () => {
    let isObject = false;
    try {
        const object = await safeAsync.add(returnsObject());
        if(!!object && object.success === 'yes') {
            isObject = true;
        }
    } catch(error) {
        console.warn(error);
    }
    expect(isObject).toBe(true);
});

test('Creates an async promise throws an exception', async () => {
    let success = false;
    try {
        await safeAsync.add(throwException());
    } catch(error) {
        if(!isSafeAsyncError(error)){
            success = true;
        }
    }
    expect(success).toBe(true);
});

test('Creates an async promise throws an safeAsyncError', async () => {
    let success = false;
    try {
        setTimeout(() => safeAsync.cancelPendingRequest())
        await safeAsync.add(throwSafeAsyncException());
    } catch(error) {
        if(isSafeAsyncError(error)){
            success = true;
        }
    }
    expect(success).toBe(true);
});